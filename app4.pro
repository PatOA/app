#-------------------------------------------------
#
# Project created by QtCreator 2016-05-12T16:06:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = app4
TEMPLATE = app


SOURCES += main.cpp\
        app4.cpp \
    fileio.cpp

HEADERS  += app4.hpp \
    fileio.hpp

FORMS    += app4.ui
