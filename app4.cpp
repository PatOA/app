#include "app4.hpp"
#include "ui_app4.h"

app4::app4(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::app4)
{
    ui->setupUi(this);
    InitGui();
    ToolTip();
}


//Destruktor f�r die Klasse App4
app4::~app4()
{
    delete ui;
}

//Funktion zur Initialisierung der GUI (definierte Status beim Start)
void app4::InitGui()
{
    //Menupunkt deaktivieren(grau hinterlegt)
    ui->actionSave->setEnabled(false);
    ui->actionSave_Ass->setEnabled(false);

    //Verbindung zwischen Objekt "ui->actionNew",Signalart,this(Instanz Klasse),SLOT-Funktion(die aufgerufen werden soll)

    connect(ui->actionNew,SIGNAL(triggered()),this,SLOT(action_New()));
    connect(ui->actionOpen,SIGNAL(triggered()),this,SLOT(action_Open()));
}


// Funktion zum Anzeigen der unterschiedlichen Tooltips

void app4::ToolTip()
{
    //ui->pushButton_New->setToolTip("Neues Textfeld");
}


//Funktion f�r ein neues "Blattpapier"
void app4::NewSheet()
{
    //Menupunkt aktivieren(bedienbar)
    ui->textEdit_Input1->setEnabled(true); 
    //Textfeld l�schen
    ui->textEdit_Input1->clear();
    ui->actionSave->setEnabled(true); 
    ui->actionSave_Ass->setEnabled(true); 
}

//Slot-Funktion die die Funktion NewSheet() aufruft - Notwendig wegen Menuaufruf
void app4::action_New()
{
    NewSheet();
}


//Slot-Funktion zum �ffnen einer Datei
void app4::action_Open()
{
    //Aufrufen der Dialogbox, Auswahl der Datei und abspeichern des Namen in "fileName" - bsp:"c:\bla\blub.txt"
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open..."),QDir::currentPath(),"*.* (*.*)");

    //�ffnen der Datei und Inhalt dieser in InputText abspeichern
    //BlaBlub.OpenFile ruft in der Klasse FileIO die Funktion "OpenFile" auf
    QString InputText = BlaBlub.OpenFile(fileName);
}
