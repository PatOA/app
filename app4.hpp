#ifndef APP4_HPP
#define APP4_HPP

#include <QMainWindow>
#include <QFileDialog>
#include "fileio.hpp"

namespace Ui {
class app4;
}

class app4 : public QMainWindow
{
    Q_OBJECT

public:
    explicit app4(QWidget *parent = 0);
    ~app4();

private slots:
    void action_New();


    void action_Open();

private:
    Ui::app4 *ui;

    void ToolTip();
    void InitGui();

    void NewSheet();

    FileIO BlaBlub;


};

#endif // APP4_HPP
